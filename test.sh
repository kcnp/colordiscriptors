#! /bin/bash

make
./colorDes.sh -d GGH -id CorelDB 4
./colorDes.sh -d GGH -id CorelDB 8
./colorDes.sh -d GSH -id CorelDB 4
./colorDes.sh -d GSH -id CorelDB 8
./colorDes.sh -d GSH -id CorelDB 16
./colorDes.sh -d GSH -id CorelDB 32
./colorDes.sh -d GSH -id CorelDB 64
./colorDes.sh -d GSH -id CorelDB 128
./colorDes.sh -d GSH -id CorelDB 256
./colorDes.sh -d CCV -id CorelDB 4 0.003
./colorDes.sh -d CCV -id CorelDB 8 0.003
./colorDes.sh -d M7CL -id CorelDB 3 3
./colorDes.sh -d M7CL -id CorelDB 6 3
./colorDes.sh -d M7CL -id CorelDB 6 6
./colorDes.sh -d M7CL -id CorelDB 10 6
./colorDes.sh -d M7CL -id CorelDB 10 10
./colorDes.sh -d M7CL -id CorelDB 15 10
./colorDes.sh -d M7CL -id CorelDB 15 15
./colorDes.sh -d M7CL -id CorelDB 21 15
./colorDes.sh -d M7CL -id CorelDB 21 21
./colorDes.sh -d M7CL -id CorelDB 28 21
./colorDes.sh -d M7CL -id CorelDB 28 28
./colorDes.sh -d M7CL -id CorelDB 64 28
./colorDes.sh -d M7CL -id CorelDB 64 64
./colorDes.sh -d JH -id CorelDB 4 ED 4 GM 4
./colorDes.sh -d JH -id CorelDB 4 ED 4 RA 4
./colorDes.sh -d JH -id CorelDB 4 ED 4 TN 4
./colorDes.sh -d JH -id CorelDB 4 GM 4 RA 4
./colorDes.sh -d JH -id CorelDB 4 GM 4 TN 4
./colorDes.sh -d JH -id CorelDB 4 RA 4 TN 4
./colorDes.sh -d ECCV -id CorelDB 4 0.003
./colorDes.sh -d ECCV -id CorelDB 8 0.003
./colorDes.sh -d JFH -id CorelDB 4
./colorDes.sh -d EH -id CorelDB 4
./colorDes.sh -d EH -id CorelDB 8
mv *.arff weka
