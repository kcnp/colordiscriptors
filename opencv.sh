#! /bin/bash

getOpenCv()
{
	#Deleta a pasta opencv e seu conteudo se ela existe
	if [ -d "opencv-2.4.5" ]
	then
		rm -r -f opencv
	fi
	
	#Se necessário faz o download do opencv apartir de um repositório sourceforge
	if ! [ -f opencv-2.4.5.tar.gz ]
	then	
		wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.5/opencv-2.4.5.tar.gz
	fi
	
	#Descomprime arquivo opencv
	tar -zxf opencv-2.4.5.tar.gz
}

compile()
{
	\cd opencv-2.4.5

	#Deleta a pasta build e seu conteudo se ela existe
	if [ -d "build" ]
	then
		rm -r -f build
	fi

	#Cria a pasta build
	mkdir build
	\cd build

	#Deleta a pasta ccache e seu conteudo se ela existe
	if [ -d "ccache" ]
	then
		rm -r -f ccache
	fi

	#Cria a pasta ccache
	mkdir ccache	

	#Seta a variável de ambiente CCACHE_DIR usada pelo ccache
	CCACHE_DIR="./ccache"
	export CCACHE_DIR

	#Compila tudo	
	cmake -DBUILD_SHARED_LIBS=OFF ..
	make -j$1

	\cd ..
	\cd ..
}

copy()
{
	#Renomeia .jar	
	\cd ./opencv-2.4.5/build/bin
	mv *.jar opencv.jar
	
	#Renomeia library	
	\cd ../lib
	mv libopencv*.so libopencv_java.so
	\cd ..
	\cd ..
	\cd ..
	
	#Se necessário cria pasta lib
	if ! [ -d "lib" ]
	then
		mkdir lib
	fi

	#Se necessário cria pasta lib
	if ! [ -d "native" ]
	then
		mkdir native
	fi

	#deleta versão anterior do jar se ela existir
	if [ -f "./lib/opencv.jar" ]
	then
		rm "./lib/opencv.jar"
	fi

	#deleta versão anterior da library se ela existir
	if [ -f "./native/libopencv_java.so" ]
	then
		rm "./native/libopencv_java.so"
	fi

	#Copia o jar e a library para a pasta lib
	cp ./opencv-2.4.5/build/bin/opencv.jar ./lib
	cp ./opencv-2.4.5/build/lib/libopencv_java.so ./native

	#rm -rf opencv-2.4.5
}

if [ $# = 1 ]
then
	compile $1
	copy
elif [ $# = 2 ]
then
	case $2 in
		--get | --g) getOpenCv ; compile $1 ; copy ;;
		*) echo "Opção $2 invalida" ;;
	esac
else
	echo "Porfavor entre com opções validas"
fi





