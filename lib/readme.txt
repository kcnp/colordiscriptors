using the mpeg7 mds library requires:


1. installing sun's jaxb:
for example, from http://java.sun.com/xml/downloads/jaxb.html

2. adding the following jars to the project:
<install>/jaxb/lib/jaxb-*.jar
<install>/shared/lib/relaxngDatatype.jar
<install>/shared/lib/xsdlib.jar

horst eidenberger (hme@ims.tuwien.ac.at) 2005
