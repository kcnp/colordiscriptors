/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * @author kcnp
 * Classe que serve para montar arquivos arff
 */
public class GenerateArff {
	private BufferedWriter warff;
	private String arffname;
	private final static String ls = System.getProperty("line.separator");
	
	public GenerateArff(String arffname,List<String> arffhead) throws IOException{
		Path parff = FileSystems.getDefault().getPath(arffname);
		this.arffname = arffname;
		try {
			this.warff = Files.newBufferedWriter(parff,Charset.defaultCharset());
		} catch (IOException e1) {
			throw new IOException("Problem open or creating the file " + arffname);
		}
			
		/*Imprimindo Header*/
		try{
			warff.write("@relation Histograma" + ls + ls);
				
			for(String s : arffhead){
				warff.write(s + ls);
			}
				
			warff.write(ls);
				
			warff.write("@data" + ls);
		}catch (IOException e) {
				throw new IOException("Problem writing in the file " + arffname);
		}
	}
	
	public void add(String args) throws IOException{
		/*Imprimindo Dados*/
		try{
			warff.write(args + ls);
		}catch (IOException e) {
			throw new IOException("Problem writing in the file " + arffname);
		}
	}
	
	public void close() throws IOException{
		try {
			this.warff.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new IOException("Problem closing the file " + arffname);
		}
	}
	
	static void gen(String arffname,List<String> arffhead,List<String> arffargs) 
			throws IOException{
		Path parff = FileSystems.getDefault().getPath(arffname);
		try(BufferedWriter warff = Files.newBufferedWriter(parff,
				Charset.defaultCharset())){
			
			/*Imprimindo Header*/
			try{
				warff.write("@relation Histograma" + ls + ls);
				
				for(String s : arffhead){
					warff.write(s + ls);
				}
				
				warff.write(ls);
				
				warff.write("@data" + ls);
			}catch (IOException e) {
				throw new IOException("Problem writing in the file " + arffname);
			}
			
			/*Imprimindo Dados*/
			try{
				for(String s : arffargs){
					warff.write(s + ls);
				}
			}catch (IOException e) {
				throw new IOException("Problem writing in the file " + arffname);
			}
		}catch (IOException e) {
			throw new IOException(e);
		}
	}
}
