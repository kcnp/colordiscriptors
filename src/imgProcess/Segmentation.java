/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgProcess;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

/**
 * @author kcnp
 *
 */
public class Segmentation {
	/**
	 * Classe comparadora que garante que o conjunto first é sempre o conjunto 
	 * de valor menor e entre dois conjunto diferentes de first o menor é 
	 * aquele que tem menor número de pixels e caso tenham mesmo número de 
	 * pixels o primeiro é o menor 
	 * 
	 * @author kcnp
	 *
	 */
	private static class ComponentesSort implements Comparator<Set<Pixel>>{
		private Collection<Pixel> first;
				
		public ComponentesSort(Collection<Pixel> first){
			this.first = first;
		}
		
		@Override
		public int compare(Set<Pixel> o1, Set<Pixel> o2) {
			if(o1.equals(o2)){
				return 0;
			}else if(this.first == o1){
				return -1;
			}else if(this.first == o2){
				return 1;
			}else{
				int res = o1.size() - o2.size();
				
				if(res != 0){
					return res;
				}else{
					return -1;
				}
			}
		}
		
	}
	
	/**
	 * Acha todas as componentes conexas
	 * @param img : Imagem a ser analisada
	 * @param lcut : O número mínimo de pixels adjacentes de cores iguais 
	 * (usando vizinhança 8) que pode ser consierado uma componente conexa
	 * @return Uma conjunto com todas as componentes conexas (cada componente
	 * conexa sendo um conjunto de pixels). o metodo first() do conjunto 
	 * retorna o conjunto de pixels considerados de fundo
	 */
	public static SortedSet<Set<Pixel>> CComponentes(Mat img,int lcut){
		Mat gmatriz = new Mat(img.size(),CvType.CV_16SC1,new Scalar(-1));
		int max = Segmentation.group(img,gmatriz);
		
		Set<Pixel> incpset = new HashSet<Pixel>(); //Conjunto de pixels incoerentes  
		SortedSet<Set<Pixel>> pixelsets = new TreeSet<Set<Pixel>>(new ComponentesSort(incpset));//Grupos 
		pixelsets.add(incpset); //Adiciona o conjunto de pixels incoerentes ao conjunto de conjuntos de pixel
			
		List<Set<Pixel>> ccset = new ArrayList<Set<Pixel>>(max);
		for(int i=0;i<max;i++){
			ccset.add(new HashSet<Pixel>());
		}
		
		for(int j=0;j<gmatriz.size().height;j++){
			for(int i=0;i<gmatriz.size().width;i++){
				short[] gnumber = new short[1];
				gmatriz.get(j,i,gnumber);
				ccset.get(gnumber[0] - 1).add(new Pixel(j,i));
			}
		}
		
		//Determina grupo de pixels incoerentes 
		for(Iterator<Set<Pixel>> it = ccset.iterator() ; it.hasNext() ; ){
			Set<Pixel> sp = it.next();
			if(sp.size() < lcut){//Grupos de pixels de tamanho menor que lcut são incoerentes
				incpset.addAll(sp); //Adiciona pixels ao conjunto de fundo
				it.remove();//Remove conjunto de pixel de fundo de conjunto de componentes conexas
			}
		}
		 
		pixelsets.addAll(ccset);//Adiciona componentes conexas a conjuntos de conjuntos de pixel
		
		return pixelsets;
	}
	
	/**
	 * Acha os grupos de pixels adjacentes de mesma cor
	 * @param img
	 * @param gmatriz
	 * @return
	 */
	private static int group(Mat img, Mat gmatriz){
		int index=0;
		for(int i=0;i<gmatriz.size().width;i++){
			for(int j=0;j<gmatriz.size().height;j++){
				if(gmatriz.get(j,i)[0] == -1){
					index++;
					group_unit(img,gmatriz,new Pixel(j,i),(short)index,new Color(img.get(j,i)));
				}
			}
		}
		
		return index;
	}
	
	/**
	 * Acha o grupo de pixels adjacentes de cor ocolor que contem o pixel start
	 * @param img
	 * @param gmatriz
	 * @param start
	 * @param index
	 * @param ocolor
	 */
	private static void group_unit(Mat img, Mat gmatriz,Pixel start,short index,Color ocolor){
		Color aux = new Color(img.get(start.y,start.x));
		if(!ocolor.cmp(aux)){
			return;
		}
		 
		gmatriz.put(start.y,start.x,new short[]{index});
		
		for(int i=-1;i<=1;i++){
			for(int j=-1;j<=1;j++){
				if(i==0 && j==0){
					continue;
				}
				
				int ny = start.y + j;
				int nx = start.x + i;
				
				if(!(nx < 0 || nx >= gmatriz.size().width || ny < 0 || 
						ny >= gmatriz.size().height)){
					if(gmatriz.get(ny,nx)[0] == -1){
						group_unit(img,gmatriz,new Pixel(ny,nx),index,ocolor);
					}
				}
			}
		}
		
		return;
	}
	
	/**
	 * 
	 * @param img => Image in the gbr format that must be looked for edges 
	 * @param bins => Number of bins that the grayscale image with the edges will be discretized
	 * @return The gray scale image that contains the edges
	 */
	public static Mat findEdges(Mat img,int bins){
		if(img.channels() != 3){
			throw new ImgProcessError("This method require a three channel image");
		}
		
		Mat img_tmp= new Mat();
		Mat gray = new Mat();
		Mat edges = new Mat();
		
		
		img.convertTo(img_tmp, CvType.CV_32F); //Converte para um formato mais preciso
		Core.normalize(img_tmp,img_tmp,0,1,Core.NORM_MINMAX);
		
		
		//Transforma a imagem colorida em tons de cinza
		Imgproc.cvtColor(img_tmp,gray,Imgproc.COLOR_BGR2GRAY);
		
		//Converte para o formato exigido para o algoritimo de canny usado para achar as arestas
		Core.normalize(gray,gray,0,255,Core.NORM_MINMAX);
		gray.convertTo(gray,CvType.CV_8U);
		
		//Pega a mediana usada para calcular os thresholds usados no algoritmo de detecção de arestas 
		int median = InfoGrayImage.median(gray,255);
		
		//Acha as arestas
		Imgproc.Canny(gray,edges,0.66*median,1.33*median);
		
		Core.normalize(edges,edges,0,bins - 1, Core.NORM_MINMAX);//Discretiza com o número de bins pedido
		
		return edges;
	}
	
}
