/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgProcess;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 * @author kcnp
 *
 */
public class InfoGrayImage {
	public static double[] histograma(Mat gray,int bins) throws ImgProcessError{
		if(gray.channels() != 1){
			throw new ImgProcessError("Gray must be on channel image");
		}
		
		if(bins <= 0){
			throw new ImgProcessError("bins must be greater than zero");
		}
		
		double hist[] = new double[bins];
		Mat norm = new Mat();
		
		gray.convertTo(norm, CvType.CV_16U);
		Core.normalize(norm,norm, 0, bins - 1, Core.NORM_MINMAX);
		
		/*Calcula o histograma*/
		short[] pixel = new short[1];
		for(int j=0;j<norm.height();j++){
			for(int i=0;i<norm.width();i++){
				norm.get(j,i,pixel);
				hist[pixel[0]]++;
			}
		}
		
		/*Normaliza*/
		int tpixels = norm.height() * norm.width();
		for(int i=0;i<bins;i++){
					hist[i] = hist[i] / tpixels;
		}

		return hist;
	}
	
	public static int median(Mat gray,int bins){
		double hist[] = histograma(gray, bins);
		
		double aux = 0;
		
		for(int i=0;i<bins;i++){
			aux = aux + hist[i];
			if(aux >= 0.5){
				return i+1;
			}
		}
		
		return bins;
	}
}
