/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgProcess;

/**
 * @author kcnp
 *
 */
public class Color {
	public double blue;
	public double green;
	public double red;
	
	
	public Color(double blue, double green, double red) {
		this.blue = blue;
		this.green = green;
		this.red = red;
	}
	
	public Color(){
		this(-1,-1,-1);
	}
	
	public Color(double[] color) {
		this(color[0],color[1],color[2]);
	}

	public boolean cmp(double blue, double green, double red){
		if(blue == this.blue && green == this.green && red == this.red){
			return true;
		}else{
			return false;
		}
	}

	public boolean cmp(double[] color) {
		return cmp(color[0],color[1],color[2]);
	}
	
	public boolean cmp(Color color){
		return cmp(color.blue,color.green,color.red);
	}
}
