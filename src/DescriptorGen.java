/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
import imgDescriptors.CCV_EHDescriptors;
import imgDescriptors.CCVector;
import imgDescriptors.ECCVector;
import imgDescriptors.EdgeHistogram;
import imgDescriptors.GGHistogram;
import imgDescriptors.GSHistogram;
import imgDescriptors.ImgDescriptor;
import imgDescriptors.ImgDescriptorError;
import imgDescriptors.JFHistogram;
import imgDescriptors.JointHistogram;
import imgDescriptors.Mpeg7CLDescriptor;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

public class DescriptorGen {
    public static void main(String[] args) throws IOException {
    	//Carregando a biblioteca nativa do opencv
    	if(File.separatorChar == '/'){//Linux
    		System.load(System.getProperty("user.dir")+ File.separator +
    				"native" + File.separator + "libopencv_java.so");
    	}else{//Windows
    		System.load(System.getProperty("user.dir")+ File.separator +
    				"native" + File.separator + "opencv_java244.dll");
    	}
    	
    	//Parser dos argumentos de linha de comando
        LineComanderParser lcp = new LineComanderParser();
        CmdLineParser clp = new CmdLineParser(lcp);
        
        try {
			clp.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			LineComanderParser.help();
			System.exit(-1);
		} catch (Exception e){
			System.err.println(e.getMessage());
			LineComanderParser.help();
			System.exit(-1);
		}
        
        //Subpastas
        String sdir[] = lcp.getImgDir().list(new FilenameFilter(){
        	@Override
			public boolean accept(File arg0, String arg1) {
				return (new File(arg0.getAbsolutePath() + File.separator + arg1)).isDirectory();
			}
        });
        	
        
        //Lista de nomes de subdiret�rios do diret�rio base
        List<String> dirnames = Arrays.asList(sdir);
        
        /*Escolha de descritor*/
        ImgDescriptor<? extends Object> desc = null;
        String FileOutBase = "";
        
        try{
	        if(lcp.getDes() == Descriptor.GGH){
	        	//Objetos para gerar os arquivos de treino e de teste
	            desc = new GGHistogram(lcp.getArg1());
	            FileOutBase = lcp.getDes().toString()+lcp.getArg1();
	        }else if(lcp.getDes() == Descriptor.GSH){
	        	//Objetos para gerar os arquivos de treino e de teste
	            desc = new GSHistogram(lcp.getArg1());
	            FileOutBase = lcp.getDes().toString()+lcp.getArg1();
	        }else if(lcp.getDes() == Descriptor.CCV){
	        	desc = new CCVector(lcp.getArg1(),lcp.getArg2D());
	        	FileOutBase = lcp.getDes().toString()+lcp.getArg1()+"_"+lcp.getArg2D();
	        }else if(lcp.getDes() == Descriptor.M7CL){
	        	desc = new Mpeg7CLDescriptor(lcp.getArg1(), lcp.getArg2I());
	        	FileOutBase = lcp.getDes().toString()+lcp.getArg1()+"_"+lcp.getArg2I();
	        }else if(lcp.getDes() == Descriptor.EH){
	        	//Objetos para gerar os arquivos de treino e de teste
	            desc = new EdgeHistogram(lcp.getArg1());
	            FileOutBase = lcp.getDes().toString()+lcp.getArg1();
	        }else if(lcp.getDes() == Descriptor.ECCV){
	        	desc = new ECCVector(lcp.getArg1(),lcp.getArg2D());
	        	FileOutBase = lcp.getDes().toString()+lcp.getArg1()+"_"+lcp.getArg2D();
	        }else if(lcp.getDes() == Descriptor.JH){
	        	//Objetos para gerar os arquivos de treino e de teste
	            desc = new JointHistogram(lcp.getArg1(),lcp.getArg2Fv(),lcp.getArg3I(),lcp.getArg4Fv(),lcp.getArg5iv());
	            FileOutBase = lcp.getDes().toString()+lcp.getArg1()+"_"+lcp.getArg2Fv()+lcp.getArg3I()+"_"+lcp.getArg4Fv()+lcp.getArg5iv();
	        }else if(lcp.getDes() == Descriptor.JFH){
	        	//Objetos para gerar os arquivos de treino e de teste
	            int bins = lcp.getArg1();
	        	desc = new JFHistogram(bins,bins,bins,bins);
	            FileOutBase = lcp.getDes().toString()+lcp.getArg1();
	        }else{
	        	desc = new CCV_EHDescriptors(lcp.getArg1(), lcp.getArg2D(),
	        			lcp.getArg3I(), lcp.getArg4I());
	        	FileOutBase = lcp.getDes().toString()+lcp.getArg1()+"_"+
	        			lcp.getArg2D()+"_"+lcp.getArg3I();
	        }
        }catch(ImgDescriptorError ide){
        	System.err.println(ide.getMessage());
        	LineComanderParser.help();
        	System.exit(-1);
        }
        
    
        //Lista de subdiret�rios do diret�rio base
        List<File> subdirs = Arrays.asList(lcp.getImgDir().listFiles(new FileFilter(){
        	@Override
			public boolean accept(File arg0) {
				return arg0.isDirectory();
			}
        }));
        
        
        /*Gerando header*/
        List<String> head = desc.printHead();
        
        String tipos = "@attribute tipo {";
        int j = 1;
        for(String t : dirnames){
                tipos = tipos + "\"" + t + "\"";
                
                if(j != dirnames.size()){
                	tipos = tipos + ",";
                }
                
                j++;
        }
        tipos = tipos + "}";
        head.add(tipos);
        
        /*Gerando arquivos arff*/
        GenerateArff training = new GenerateArff(FileOutBase+"training.arff",head);
        GenerateArff test = new GenerateArff(FileOutBase+"test.arff",head);
        
        //Popula os arquivos de treino e de teste iterando sobre as subpastas
        for(File subdir : subdirs){
        	List<String> imgfiles = Arrays.asList(subdir.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return ((new File(dir.getAbsolutePath() + File.separator + name)).isFile() 
							&& name.length() > 4 && name.substring(name.length()-4).equals(".jpg"));
				}
			}));
        	long n = imgfiles.size();
        	long t = (long) Math.floor(n * 0.8);
        	long i = 1;
        	
        	for(String img : imgfiles){
        		if(i <= t){//imagens de treinamento
        			training.add(desc.printDescriptor(subdir.getAbsolutePath() + 
        					File.separator + img) + subdir.getName());
        			i++;
        		}else{
        			//Imagens de teste
        			test.add(desc.printDescriptor(subdir.getAbsolutePath() + 
        					File.separator + img) + subdir.getName());
        		}
        		
        		System.out.println(img);
        	}
        }
        
        training.close();
        test.close();
    }
}
