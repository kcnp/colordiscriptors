/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
import imgDescriptors.localPixelsFeatures.Feature;

import java.io.File;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;


enum Descriptor { GGH,GSH,CCV,M7CL,CCV_EH,EH,ECCV,JH,JFH }
/**
 * @author kcnp
 *
 */
public class LineComanderParser {
	
	private Descriptor des;
	private File imgDir;
	private String arg1 = "";
	private int arg1iv;
	private String arg2 = "";
	private double arg2dv;
	private int arg2iv;
	private Feature arg2Fv;
	private String arg3 = "";
	private int arg3iv;
	private String arg4 = "";
	private int arg4iv;
	private Feature arg4Fv;
	private String arg5 = "";
	private int arg5iv;
	
	public LineComanderParser(){
		this.arg1iv = -1;
		this.arg2iv = -1;
		this.arg2dv = -1;
		this.arg2Fv = Feature.NONE;
		this.arg3iv = -1;
		this.arg4iv = -1;
		this.setArg4Fv(Feature.NONE);
		this.arg5iv = -1;
	}
	
	@Option(name = "-d",aliases = {"--descriptors"},required = true,usage = "Descritor requisitado")
	public void setDes(Descriptor des) {
		this.des = des;
	}

	@Option(name = "-id",aliases = {"--imgDir"},required = true,
			usage = "Nome do diretório onde estão os sub diretórios que representão os classes de imagem" )
	public void setImgDir(File imgDir) throws Exception {
		if(imgDir.isDirectory()){
			this.imgDir = imgDir;
		}else{
			throw new Exception("Diretório não existe");
		}
	}

	@Argument(index=0,required=false)
	public void setArg1(String arg1) throws Exception {
		this.arg1 = arg1;
		
		if(this.des == Descriptor.GGH || this.des == Descriptor.GSH || 
				this.des == Descriptor.CCV || this.des == Descriptor.M7CL || 
				this.des == Descriptor.CCV_EH || this.des == Descriptor.EH || 
				this.des == Descriptor.ECCV || this.des == Descriptor.JH ||
				this.des == Descriptor.JFH){
			try{
				arg1iv = Integer.valueOf(this.arg1);
			}catch(NumberFormatException n){
				if(this.des == Descriptor.M7CL){
					throw new Exception(
							"The values of numberofYCoeff must be one of the " +
							"integers in the set {3,6,10,15,21,28,64}"
					);
				}else{
					throw new Exception("Please enter with a integer to the number of bins");
				}
			}
		}
	}

	@Argument(index=1,required=false)
	public void setArg2(String arg2) throws Exception{
		this.arg2 = arg2;
		
		if(this.des == Descriptor.CCV || this.des == Descriptor.CCV_EH || this.des == Descriptor.ECCV){
			try{
				arg2dv = Double.valueOf(this.arg2);
			}catch(NumberFormatException n){
				throw new Exception("Please enter with a double value in the" +
						" interval (0,1] to be threshold of the definition of a" +
						" connected component");
			}
		}else if(this.des == Descriptor.M7CL){
			try{
				arg2iv = Integer.valueOf(this.arg2);
			}catch(NumberFormatException n){
				throw new Exception(
						"The values of numberofCCoeff must be one of the " +
						"integers in the set {3,6,10,15,21,28,64}"
				);
			}
		}else if(this.des == Descriptor.JH){
			try{
				this.arg2Fv = Feature.valueOf(arg2);
			}catch(IllegalArgumentException ex){
				throw new Exception("Wrong fetaure F1");
			}
		}else{
			throw new Exception("Argumentos excessivos para opção " + this.des.toString());
		}
	}
	
	@Argument(index=2,required=false)
	public void setArg3(String arg3) throws Exception{
		this.arg3 = arg3;
		
		if(this.des == Descriptor.CCV_EH || this.des == Descriptor.JH){
			try{
				arg3iv = Integer.valueOf(this.arg3);
			}catch(NumberFormatException n){
				throw new Exception(
					"The values of BINS must be in the interval [1,256]"
				);
			}
		}else{
			throw new Exception("Argumentos excessivos para opção " + this.des.toString());
		}
	}
	
	@Argument(index=3,required=false)
	public void setArg4(String arg4) throws Exception{
		this.arg4 = arg4;
		
		if(this.des == Descriptor.JH){
			try{
				this.setArg4Fv(Feature.valueOf(this.arg4));
			}catch(IllegalArgumentException ex){
				throw new Exception("Wrong fetaure F2");
			}
		}else{
			throw new Exception("Argumentos excessivos para opção " + this.des.toString());
		}
	}
	
	@Argument(index=4,required=false)
	public void setArg5(String arg5) throws Exception{
		this.arg5 = arg5;
		
		if(this.des == Descriptor.JH){
			try{
				arg5iv = Integer.valueOf(this.arg5);
			}catch(NumberFormatException n){
				throw new Exception(
					"The values of BINS must be in the interval [1,256]"
				);
			}
		}else{
			throw new Exception("Argumentos excessivos para opção " + this.des.toString());
		}
	}
	
	public Descriptor getDes() {
		return des;
	}

	public File getImgDir() {
		return imgDir;
	}

	public int getArg1() {
		return arg1iv;
	}
	
	public int getArg2I(){
		return arg2iv;
	}
	
	public double getArg2D(){
		return arg2dv;
	}
	
	public int getArg3I(){
		return arg3iv;
	}
	
	public int getArg4I(){
		return arg4iv;
	}
	
	public static void help(){
		System.out.println("\nOptions:");
		System.out.println("\tGlobal Gruped Histogram: -d GGH -id ImgDir BINS");
		System.out.println("\tGlobal Separated Histogram: -d GSH -id ImgDir BINS");
		System.out.println("\tColor Coherence Vector: -d CCV -id ImgDir BINS THRESHOLD");
		System.out.println("\tEdge Color Coherence Vector: -d ECCV -id ImgDir BINS THRESHOLD");
		System.out.println("\tMpeg-7 Color layout: -d M7CL -id ImgDir " +
				"NUMBER_OF_YCOEFF NUMBER_OF_CCOEF");
		System.out.println("\tJoint Feature Histogram: -d JFH -id ImgDir BINS");
		System.out.println("\tJoint Histogram: -d JH -id ImgDir " +
				"BINS FEATURE1 BISN2 FEATURE2");
		System.out.println("\t\tPossible Features:");
		System.out.println("\t\t\tEdge Density: ED");
		System.out.println("\t\t\tGradient Magnitude: GM");
		System.out.println("\t\t\tTexturedness: TN");
		System.out.println("\t\t\tRank: RA");
	}

	/**
	 * @return the arg4Fv
	 */
	public Feature getArg4Fv() {
		return arg4Fv;
	}

	/**
	 * @param arg4Fv the arg4Fv to set
	 */
	public void setArg4Fv(Feature arg4Fv) {
		this.arg4Fv = arg4Fv;
	}
	
	public Feature getArg2Fv() {
		return arg2Fv;
	}

	public int getArg5iv() {
		return arg5iv;
	}
}
