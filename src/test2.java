/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
import imgDescriptors.localPixelsFeatures.EdgeDensity;
import imgDescriptors.localPixelsFeatures.PixelFeature;

import java.io.File;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

/**
 * 
 */

/**
 * @author kcnp
 *
 */
public class test2 {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		//Carregando a biblioteca nativa do opencv
    	if(File.separatorChar == '/'){//Linux
    		System.load(System.getProperty("user.dir")+ File.separator +
    				"native" + File.separator + "libopencv_java.so");
    	}else{//Windows
    		System.load(System.getProperty("user.dir")+ File.separator +
    				"native" + File.separator + "opencv_java244.dll");
    	}
		
		PixelFeature e = new EdgeDensity(255,4,5);
		Mat m = e.getFeature("435001.jpg");
		System.out.println(m.dump());
		
		Highgui.imwrite("saida.jpeg",m);
	}

}
