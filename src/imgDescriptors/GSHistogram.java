/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

/**
 * @author kcnp
 *
 */
public class GSHistogram implements ImgDescriptor<double [][]> {
	private int bins;
	
	/**
	 * @param bins - Number of bins for each channel
	 * @throws ImgDescriptorError
	 */
	public GSHistogram(int bins) throws ImgDescriptorError{
		if(bins > 0 && bins <= 256){
			this.bins = bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
	}
	
	@Override
	public double[][] getDescriptor(String img) {
		Mat m = Highgui.imread(img);
		Mat norm = new Mat(); 
		Core.normalize(m,norm,0,this.bins - 1, Core.NORM_MINMAX);
		norm.convertTo(norm,CvType.CV_16U);
		
		/*Inicializa histograma*/
		double[][] hist = new double[3][this.bins];
		for(int i=0;i<3;i++){
			for(int j=0;j<this.bins;j++){
				hist[i][j] = 0;
			}
		}
		
		/*Calcula o histograma*/
		short[] pixel = new short[3];
		for(int j=0;j<norm.height();j++){
			for(int i=0;i<norm.width();i++){
				norm.get(j,i,pixel);
				hist[0][pixel[2]]++;
				hist[1][pixel[1]]++;
				hist[2][pixel[0]]++;
			}
		}
		
		/*Normalize*/
		int tpixels = norm.height() * norm.width();
		for(int i=0;i<3;i++){
			for(int j=0;j<this.bins;j++){
				hist[i][j] = hist[i][j] / tpixels;
			}
		}
		
		return hist;
	}

	@Override
	public List<String> printHead() {
			List<String> head = new ArrayList<String>(this.bins * 3);
			
			for(int i=1;i<=this.bins;i++){
				head.add("@attribute red" + i + " numeric");
			}
			
			for(int i=1;i<=this.bins;i++){
				head.add("@attribute green" + i + " numeric");
			}
			
			for(int i=1;i<=this.bins;i++){
				head.add("@attribute blue" + i + " numeric");
			}
			
			return head;
	}

	@Override
	public String printDescriptor(String img){
		double [][] hist = this.getDescriptor(img);
		
		String args = "";
		for(int i=0;i<3;i++){
			for(int j=0;j<this.bins;j++){
				args = args + (hist[i][j] + ",");
			}
		}
		
		return args;
	}
}
