/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import imgProcess.Pixel;
import imgProcess.Segmentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 * @author kcnp
 *
 */
public class ECCVector implements ImgDescriptor<double[][][][]> {
	private int rgb_bins;
	private double lcut;
	
	public ECCVector(int rgb_bins,double lcut) throws ImgDescriptorError{
		if(rgb_bins > 0 && rgb_bins <= 256){
			this.rgb_bins = rgb_bins;
		}else{
			throw new ImgDescriptorError("Rgb_bins must be in the interval [1,256]");
		}
		
		if(lcut > 0 && lcut <=1){
			this.lcut = lcut;
		}else{
			throw new ImgDescriptorError("Lcut should be in the interval (0,1]");
		}
		
	}
	
	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#getDescriptor(java.lang.String)
	 */
	@Override
	public double[][][][] getDescriptor(String img) {
		Mat mimg = Highgui.imread(img);
		Mat bgauss = new Mat();
		Mat norm = new Mat();
		
		//Aplica filtro gaussiano de suavização
		Imgproc.GaussianBlur(mimg,bgauss,new Size(5,5),0,0);//Suaviza com filtro gaussiano
		
		//Discretiza o número de cores para bins^3
		Core.normalize(bgauss, norm, 0, this.rgb_bins - 1, Core.NORM_MINMAX);
		norm.convertTo(norm,CvType.CV_16U);
		
		//Calcula número de pixels total da imagem
		int tnpixels = (int)(norm.size().width * norm.size().height);
		
		//Descobre as componentes conexas
		SortedSet<Set<Pixel>> conj = Segmentation.CComponentes(norm, (int)(Math.round(this.lcut * tnpixels)));

		//Inicialização dos histogramas
		double[][][][] hists = new double[3][this.rgb_bins][this.rgb_bins][this.rgb_bins];
		for(int i=0;i<2;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					for(int w=0;w<this.rgb_bins;w++){
						hists[i][j][k][w] = 0;
					}
				}
			}
		}
		
		short[] pcolor = new short[3];
		
		//Calcula histograma dos pixels incoerentes(de fundo)
		for(Pixel p: conj.first()){
			norm.get(p.y,p.x,pcolor);
			hists[0][pcolor[2]][pcolor[1]][pcolor[0]]++;
		}
		
		int fnpixels = conj.first().size();
		if(fnpixels == 0){
			fnpixels = 1;
		}
		for(int i=0;i<this.rgb_bins;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					hists[0][i][j][k] = hists[0][i][j][k] / fnpixels;
				}
			}
		}
		
		Mat edges = Segmentation.findEdges(norm,2); //Acha as arestas
		
		//Calcula histograma dos pixels que fazem parte de alguma componente conexa
		//Divididos em dois grupo, uma das aresta e outro dos pixels não arestas
		int ccenpixels = 0;
		int ccnenpixels = 0;
		
		conj.remove(conj.first());
		for(Set<Pixel> s: conj){
			for(Pixel p : s){
				norm.get(p.y,p.x,pcolor);
				
				if(edges.get(p.y,p.x)[0] == 1){
					hists[1][pcolor[2]][pcolor[1]][pcolor[0]]++;
					ccenpixels++;
				}else{
					hists[2][pcolor[2]][pcolor[1]][pcolor[0]]++;
					ccnenpixels++;
				}
			}
		}
		
		if(ccenpixels == 0){
			ccenpixels = 1;
		}
		
		if(ccnenpixels == 0){
			ccnenpixels = 1;
		}
		
		for(int i=0;i<this.rgb_bins;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					hists[1][i][j][k] = hists[1][i][j][k] / ccenpixels;
					hists[2][i][j][k] = hists[2][i][j][k] / ccnenpixels;
				}
			}
		}
		
		return hists;
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#printHead()
	 */
	@Override
	public List<String> printHead() {
		int ncolors = this.rgb_bins * this.rgb_bins * this.rgb_bins;
		List<String> head = new ArrayList<String>(ncolors);
		
		for(int i=1;i<=ncolors;i++){
			head.add("@attribute incoerente_cor" + i + " numeric");
		}
		
		for(int i=1;i<=ncolors;i++){
			head.add("@attribute edge_coerente_cor" + i + " numeric");
		}
		
		for(int i=1;i<=ncolors;i++){
			head.add("@attribute no_edge_coerente_cor" + i + " numeric");
		}
		
		return head;
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#printDescriptor(java.lang.String)
	 */
	@Override
	public String printDescriptor(String img) {
		double [][][][] hist = this.getDescriptor(img);
		
		String args = "";
		for(int i=0;i<3;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					for(int w=0;w<this.rgb_bins;w++){
						args = args + hist[i][j][k][w] + ",";
					}
				}
			}
		}
		
		return args;
	}
}
