/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import java.util.List;

/**
 * @author kcnp
 *
 */
public class CCV_EHDescriptors implements ImgDescriptor<double []> {
	private int ccv_bins;
	private int eh_bins;
	private CCVector ccv;
	private EdgeHistogram eh;
	
	
	public CCV_EHDescriptors(int ccv_bins,double lcut,int eh_bins,
			int numberOfCCoeff) throws ImgDescriptorError{
		this.ccv_bins = ccv_bins;
		this.eh_bins = eh_bins;
		ccv = new CCVector(ccv_bins,lcut);
		eh = new EdgeHistogram(eh_bins);
	}

	@Override
	public double[] getDescriptor(String img) {
		double ccvv[][][][] = ccv.getDescriptor(img);
		double ehv[][][] = eh.getDescriptor(img);
		
		int tam = this.ccv_bins*this.ccv_bins*this.ccv_bins*2 + this.eh_bins*this.eh_bins*this.eh_bins;
		double des[] = new double[tam];
		
		int ind = 0;
		
		for(int i=0;i<2;i++){
			for(int j=0;j<this.ccv_bins;j++){
				for(int k=0;k<this.ccv_bins;k++){
					for(int w=0;w<this.ccv_bins;w++){
						des[ind] = ccvv[i][j][k][w];
						ind++;
					}
				}
			}
		}
		
		for(int i=0;i<this.eh_bins;i++){
			for(int j=0;j<this.eh_bins;j++){
				for(int k=0;k<this.eh_bins;k++){
					des[ind] = ehv[i][j][k];
					ind++;
				}
			}
		}
		
		return des;
	}

	@Override
	public List<String> printHead() {
		List<String> head = this.ccv.printHead();
		head.addAll(this.eh.printHead());
		return head;
	}

	@Override
	public String printDescriptor(String img) {
		return (this.ccv.printDescriptor(img) + this.eh.printDescriptor(img));
	}

}
