/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import imgDescriptors.localPixelsFeatures.EdgeDensity;
import imgDescriptors.localPixelsFeatures.Feature;
import imgDescriptors.localPixelsFeatures.GradientMagnitude;
import imgDescriptors.localPixelsFeatures.PixelFeature;
import imgDescriptors.localPixelsFeatures.Rank;
import imgDescriptors.localPixelsFeatures.Texturedness;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

/**
 * @author kcnp
 *
 */


public class JointHistogram implements ImgDescriptor<double[][][][][]> {
	private int rgb_bins;
	private PixelFeature pf1;
	private PixelFeature pf2;
	private int pf1_bins;
	private int pf2_bins;
	
	
	/**
	 * 
	 * @param rgb_bins
	 * @param pf1
	 * @param pf1_bins
	 * @param pf2
	 * @param pf2_bins
	 * @throws ImgDescriptorError
	 */
	public JointHistogram(int rgb_bins,Feature pf1,int pf1_bins,Feature pf2,int pf2_bins) throws ImgDescriptorError{
		if(rgb_bins > 0 && rgb_bins <= 256){
			this.rgb_bins = rgb_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		if(pf1_bins > 0 && pf1_bins <= 256){
			this.pf1_bins = rgb_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		if(pf2_bins > 0 && pf2_bins <= 256){
			this.pf2_bins = pf2_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		if(pf1 == Feature.ED){
			this.pf1 = new EdgeDensity(255,this.pf1_bins,5);
		}else if(pf1 == Feature.GM){
			this.pf1 = new GradientMagnitude(255,this.pf1_bins,7);
		}else if(pf1 == Feature.RA){
			this.pf1 = new Rank(255,this.pf1_bins,5);
		}else if(pf1 == Feature.TN){
			this.pf1 = new Texturedness(255,this.pf1_bins,5,20);
		}else{
			throw new ImgDescriptorError("Wrong Feature Value");
		}
		
		if(pf2 == Feature.ED){
			this.pf2 = new EdgeDensity(255,this.pf2_bins,5);
		}else if(pf2 == Feature.GM){
			this.pf2 = new GradientMagnitude(255,this.pf2_bins,7);
		}else if(pf2 == Feature.RA){
			this.pf2 = new Rank(255,this.pf2_bins,5);
		}else if(pf2 == Feature.TN){
			this.pf2 = new Texturedness(255,this.pf2_bins,5,20);
		}else{
			throw new ImgDescriptorError("Wrong Feature Value");
		}
		
	}
	
	@Override
	public double[][][][][] getDescriptor(String img) {
		Mat mimg = Highgui.imread(img);
		Mat norm = new Mat(); 
		Core.normalize(mimg,norm,0,this.rgb_bins - 1, Core.NORM_MINMAX);
		norm.convertTo(norm,CvType.CV_16U);
		
		Mat mf1 = this.pf1.getFeature(img);
		Mat mf2 = this.pf2.getFeature(img);
		
		/*Inicializa histograma*/
		double[][][][][] hist = new double[this.rgb_bins][this.rgb_bins][this.rgb_bins][this.pf1_bins][this.pf2_bins];
		for(int i=0;i<this.rgb_bins;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					for(int l=0;l<this.pf1_bins;l++){
						for(int m=0;m<this.pf2_bins;m++){
							hist[i][j][k][l][m] = 0;
						}
					}	
				}
			}
		}
		
		/*Calcula o histograma*/
		short[] pixel = new short[3];
		int vf1;
		int vf2;
		for(int j=0;j<norm.height();j++){
			for(int i=0;i<norm.width();i++){
				norm.get(j,i,pixel);
				vf1 = (int) (mf1.get(j,i)[0]);
				vf2 = (int) (mf2.get(j,i)[0]);
				hist[pixel[2]][pixel[1]][pixel[0]][vf1][vf2]++;
			}
		}
		
		/*Normaliza*/
		int tpixels = norm.height() * norm.width();
		for(int i=0;i<this.rgb_bins;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					for(int l=0;l<this.pf1_bins;l++){
						for(int m=0;m<this.pf2_bins;m++){
							hist[i][j][k][l][m] /= tpixels;
						}
					}	
				}
			}
		}
		
		return hist;
	}

	@Override
	public List<String> printHead() {
		int nfeatures = this.rgb_bins * this.rgb_bins * this.rgb_bins * this.pf1_bins * this.pf2_bins;
		List<String> head = new ArrayList<String>(nfeatures);
			
		for(int i=1;i<=nfeatures;i++){
			head.add("@attribute feature" + i + " numeric");
		}
		
		return head;
	}

	@Override
	public String printDescriptor(String img) {
		double [][][][][] hist = this.getDescriptor(img);
		
		String args = "";
		
		for(int i=0;i<this.rgb_bins;i++){
			for(int j=0;j<this.rgb_bins;j++){
				for(int k=0;k<this.rgb_bins;k++){
					for(int l=0;l<this.pf1_bins;l++){
						for(int m=0;m<this.pf2_bins;m++){
							args = args + (hist[i][j][k][l][m]+",");
						}
					}	
				}
			}
		}

		return args;
	}

}
