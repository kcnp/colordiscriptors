/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors.localPixelsFeatures;

import imgDescriptors.ImgDescriptorError;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 * @author kcnp
 *
 */
public class Texturedness extends PixelFeature {
	private double threshold;
	private int mask_tam;
	
	/**
	 * @param rgb_bins
	 * @param feature_bins
	 * @param mask_tam
	 * @param threshold
	 * @throws ImgDescriptorError
	 */
	public Texturedness(int rgb_bins,int feature_bins,int mask_tam,double threshold) throws ImgDescriptorError {
		super(rgb_bins,feature_bins);
		
		if((mask_tam % 2) == 1 && mask_tam >= 3){
			this.mask_tam = mask_tam / 2;
		}else{
			throw new ImgDescriptorError("Mask_tam must be odd and greater than or equal to 3");
		}
		
		if(threshold >= 0 && threshold <= 255*Math.sqrt(3)){
			this.threshold = threshold;
		}else{
			throw new ImgDescriptorError("Threshold must be in the interval [0,255]");
		}
	}

	/**
	 * @Override
	 */
	protected Mat getDescriptor(Mat m) throws ImgDescriptorError{
		if(m.depth() != CvType.CV_8U){
			throw new ImgDescriptorError("Image must have the depth CV_8U");
		}else if(m.channels() != 3){
			throw new ImgDescriptorError("Image must have three channels (bgr image)");
		}
		
		Mat res = new Mat(m.size(),CvType.CV_32FC1);
		
		//Calcula a feature
		for(int j=0;j<m.height();j++){
			for(int i=0;i<m.width();i++){
				double pixel[] = m.get(j,i);
				double feature = 0;
				
				for(int k=-(this.mask_tam);k<=this.mask_tam;k++){
					for(int w=-(this.mask_tam);w<=this.mask_tam;w++){
						int y = j + k;
						int x = i + w;
						
						if(y < 0 || x < 0 || y >= m.height() || x >= m.width()){
							continue;
						}else{
							double disb = pixel[0] - m.get(y,x)[0];
							double disg = pixel[1] - m.get(y,x)[1];
							double disr = pixel[2] - m.get(y,x)[2];
									
							double dis = Math.sqrt(disb*disb + disg*disg + disr*disr);
							if(dis > this.threshold){
								feature++;
							}
						}
					}
				}
				
				res.put(j,i,feature);
			}
		}
		
		Core.normalize(res,res,0,this.getFeature_bins() - 1,Core.NORM_MINMAX);
		res.convertTo(res,CvType.CV_8U);
		return res;
	}
}
