/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors.localPixelsFeatures;

import imgDescriptors.ImgDescriptorError;
import imgProcess.Segmentation;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 * @author kcnp
 * Calcula a feature Edge density para cada pixel
 */
public class EdgeDensity extends PixelFeature {
	private int mask_tam;
	
	/**
	 * 
	 * @param rgb_bins
	 * @param feature_bins
	 * @param mask_tam
	 * @throws ImgDescriptorError
	 */
	public EdgeDensity(int rgb_bins,int feature_bins,int mask_tam) throws ImgDescriptorError{
		super(rgb_bins,feature_bins);
		
		if((mask_tam % 2) == 1 && mask_tam >= 3){
			this.mask_tam = mask_tam / 2;
		}else{
			throw new ImgDescriptorError("Mask_tam must be odd and greater than  or equal to 3");
		}
	}

	/**
	 * @Override
	 */
	protected Mat getDescriptor(Mat m) throws ImgDescriptorError{
		if(m.depth() != CvType.CV_8U){
			throw new ImgDescriptorError("Image must have the depth CV_8U");
		}else if(m.channels() != 3){
			throw new ImgDescriptorError("Image must have three channels (bgr image)");
		}
		
		Mat edges = Segmentation.findEdges(m,2); //Acha as arestas
		Mat res = new Mat(edges.size(),CvType.CV_32FC1); //Matriz que conterá a saida
		
		//Calcula densidade de arestas
		for(int j=0;j<edges.height();j++){
			for(int i=0;i<edges.width();i++){
				int edge_pixels = 0;
				
				for(int k=-(this.mask_tam);k<=this.mask_tam;k++){
					for(int w=-(this.mask_tam);w<=this.mask_tam;w++){
						int y = j + k;
						int x = i + w;
						
						if(y < 0 || x < 0 || y >= edges.height() || x >= edges.width()){
							continue;
						}else{
							if(edges.get(y,x)[0] == 1){
								edge_pixels++;
							}
						}
					}
				}
				
				res.put(j,i,edge_pixels);
			}
		}
		
		Core.normalize(res,res,0,this.getFeature_bins() - 1,Core.NORM_MINMAX);
		res.convertTo(res,CvType.CV_8U);
		
		return res;
	}

}
