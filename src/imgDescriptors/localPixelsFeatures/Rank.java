/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors.localPixelsFeatures;

import imgDescriptors.ImgDescriptorError;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * @author kcnp
 *
 */
public class Rank extends PixelFeature {
	private int mask_tam;
	
	public Rank(int rgb_bins, int feature_bins, int mask_tam) throws ImgDescriptorError {
		super(rgb_bins, feature_bins);
		
		if((mask_tam % 2) == 1 && mask_tam >= 3){
			this.mask_tam = mask_tam / 2;
		}else{
			throw new ImgDescriptorError("Mask_tam must be odd and greater than  or equal to 3");
		}
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.localPixelsFeatures.PixelFeature#getDescriptor(org.opencv.core.Mat)
	 */
	@Override
	protected Mat getDescriptor(Mat m) throws ImgDescriptorError {
		if(m.depth() != CvType.CV_8U){
			throw new ImgDescriptorError("Image must have the depth CV_8U");
		}else if(m.channels() != 3){
			throw new ImgDescriptorError("Image must have three channels (bgr image)");
		}
		
		Mat mgaussian = new Mat();
		Mat res = new Mat(m.size(),CvType.CV_32FC1);
		
		//Suaviza com filtro gaussiano
		Imgproc.GaussianBlur(m,mgaussian,new Size(2*this.mask_tam + 1,2*this.mask_tam + 1),0,0);
		
		//Calcula densidade de arestas
		for(int j=0;j<m.height();j++){
			for(int i=0;i<m.width();i++){
				double pixel[] = mgaussian.get(j,i);
				double pixel_int = Math.sqrt(
						pixel[0]*pixel[0] + pixel[1]*pixel[1] + pixel[2]*pixel[2]
				);
				double feature = 0;
				
				for(int k=-(this.mask_tam);k<=this.mask_tam;k++){
					for(int w=-(this.mask_tam);w<=this.mask_tam;w++){
						int y = j + k;
						int x = i + w;
						
						if(y < 0 || x < 0 || y >= m.height() || x >= m.width()){
							continue;
						}else{
							double npixel[] = mgaussian.get(y,x);
							double npixel_int = Math.sqrt(
									npixel[0]*npixel[0] + npixel[1]*npixel[1] + 
									npixel[2]*npixel[2]
							);
									
							if(npixel_int < pixel_int){
								feature++;
							}
						}
					}
				}
				
				res.put(j,i,feature);
			}
		}
		
		Core.normalize(res,res,0,this.getFeature_bins() - 1,Core.NORM_MINMAX);
		res.convertTo(res,CvType.CV_8U);
		return res;
	}

}
