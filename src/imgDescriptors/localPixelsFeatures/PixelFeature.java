/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors.localPixelsFeatures;

import imgDescriptors.ImgDescriptorError;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;


/**
 * @author kcnp
 * Extrai a matriz de uma feature referente a cada pixel (Matriz deve ser das mesmas dimensões da imagem)
 */
public abstract class PixelFeature{
	private int rgb_bins;
	private int feature_bins;
	
	/**
	 * @param m => The bgr matrix of the image from where the feature must be extracted
	 * @return
	 * @throws ImgDescriptorError
	 */
	abstract protected Mat getDescriptor(Mat m) throws ImgDescriptorError;
	
	/**
	 * @param rgb_bins => Number of bins to use in the discretization of the color space
	 * @param feature_bins => Number of bins to use in the discretization of the feature
	 * @throws ImgDescriptorError
	 */
	public PixelFeature(int rgb_bins,int feature_bins) throws ImgDescriptorError{
		if(rgb_bins > 0 && rgb_bins <= 256){
			this.rgb_bins = rgb_bins;
		}else{
			throw new ImgDescriptorError("Rgb_bins must be in the interval [1,256]");
		}
		
		if(feature_bins > 0 && feature_bins <= 256){
			this.feature_bins = feature_bins;
		}else{
			throw new ImgDescriptorError("the feature_bins must be in the interval [1,256]");
		}
	}
	
	/**
	 * 
	 * @param img => Imagem da qual quer-se extrair a feature
	 * @return Matriz com mesma dimensão da imagem, com o valor da feature em cada pixel
	 * @throws ImgDescriptorError
	 */
	public Mat getFeature(String img) throws ImgDescriptorError{
		Mat mimg = Highgui.imread(img);
		Mat norm = new Mat();
		
		//Discretiza o número de cores para bins^3
		Core.normalize(mimg, norm, 0, this.rgb_bins - 1, Core.NORM_MINMAX);
		norm.convertTo(norm,CvType.CV_8U);
		
		Mat res = this.getDescriptor(norm); 
		 
		if(res.height() == mimg.height() && res.width() == mimg.width()){
			return res;
		}else{
			throw new ImgDescriptorError("Local feature matriz must be of the image size");
		}
	}

	
	public int getRgb_Bins() {
		return rgb_bins;
	}

	/**
	 * @return the feature_bins
	 */
	public int getFeature_bins() {
		return feature_bins;
	}
}
