/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors.localPixelsFeatures;

import imgDescriptors.ImgDescriptorError;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * @author kcnp
 *
 */
public class GradientMagnitude extends PixelFeature {
	private int kernel_size;
	
	/**
	 * 
	 * @param rgb_bins
	 * @param feature_bins
	 * @param kernel_size
	 * @throws ImgDescriptorError
	 */
	public GradientMagnitude(int rgb_bins,int feature_bins,int kernel_size) throws ImgDescriptorError {
		super(rgb_bins,feature_bins);
		
		if(kernel_size == 1 || kernel_size == 3 || kernel_size == 5 || kernel_size == 7){
			this.kernel_size = kernel_size;
		}else{
			throw new ImgDescriptorError("Kernel size must be 1,3,5 or 7");
		}
	}

	/**
	 *@Override
	 */
	protected Mat getDescriptor(Mat m) throws ImgDescriptorError {
		if(m.depth() != CvType.CV_8U){
			throw new ImgDescriptorError("Image must have the depth CV_8U");
		}else if(m.channels() != 3){
			throw new ImgDescriptorError("Image must have three channels (bgr image)");
		}
		
		/*Transforma a imagem em tons de cinza*/
		Mat gray = new Mat(m.size(),CvType.CV_8UC1);
		Imgproc.cvtColor(m,gray,Imgproc.COLOR_BGR2GRAY);
		
		/*Usa o algoritmo de calculo de gradiente sobel*/
		Mat res = new Mat(m.size(),CvType.CV_32FC1);
		Imgproc.Sobel(m,res,CvType.CV_32F,1,1,this.kernel_size,1,0,Imgproc.BORDER_DEFAULT);
		
		Core.normalize(res,res,0,this.getFeature_bins() - 1,Core.NORM_MINMAX);
		res.convertTo(res,CvType.CV_8U);
		
		return res;
	}
}
