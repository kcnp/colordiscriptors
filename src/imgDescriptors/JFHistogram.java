/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import imgDescriptors.localPixelsFeatures.EdgeDensity;
import imgDescriptors.localPixelsFeatures.GradientMagnitude;
import imgDescriptors.localPixelsFeatures.Rank;
import imgDescriptors.localPixelsFeatures.Texturedness;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;

/**
 * @author kcnp
 *Joint feature histogram
 */
public class JFHistogram implements ImgDescriptor<double[][][][]> {
	private EdgeDensity ed;
	private GradientMagnitude gm;
	private Rank ra;
	private Texturedness tn;
	private int ed_bins;
	private int gm_bins;
	private int ra_bins;
	private int tn_bins;
	
	/**
	 * @param bins - Number of bins for each channel
	 * @throws ImgDescriptorError
	 */
	public JFHistogram(int ed_bins,int gm_bins,int ra_bins,int tn_bins) throws ImgDescriptorError{
		if(ed_bins > 0 && ed_bins <= 256){
			this.ed_bins = ed_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		if(gm_bins > 0 && gm_bins <= 256){
			this.gm_bins = gm_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		if(ra_bins > 0 && ra_bins <= 256){
			this.ra_bins = ra_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		if(tn_bins > 0 && tn_bins <= 256){
			this.tn_bins = tn_bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
		
		ed = new EdgeDensity(255,this.ed_bins,5);
		gm = new GradientMagnitude(255,this.gm_bins,7);
		ra = new Rank(255,this.ra_bins,5);
		tn = new Texturedness(255,this.tn_bins,5,20);
		
	}
	
	@Override
	public double[][][][] getDescriptor(String img) {
		Mat med = this.ed.getFeature(img);
		Mat mgm = this.gm.getFeature(img);
		Mat mra = this.ra.getFeature(img);
		Mat mtn = this.tn.getFeature(img);
		
		/*Inicializa histograma*/
		double[][][][] hist = new double[this.ed_bins][this.gm_bins][this.ra_bins][this.tn_bins];
		for(int i=0;i<this.ed_bins;i++){
			for(int j=0;j<this.gm_bins;j++){
				for(int k=0;k<this.ra_bins;k++){
					for(int l=0;l<this.tn_bins;l++){
						hist[i][j][k][l] = 0;
					}	
				}
			}
		}
		
		/*Calcula o histograma*/
		int ved;
		int vgm;
		int vra;
		int vtn;
		for(int j=0;j<med.height();j++){
			for(int i=0;i<med.width();i++){
				ved = (int) (med.get(j,i)[0]);
				vgm = (int) (mgm.get(j,i)[0]);
				vra = (int) (mra.get(j,i)[0]);
				vtn = (int) (mtn.get(j,i)[0]);
				hist[ved][vgm][vra][vtn]++;
			}
		}
		
		/*Normaliza*/
		int tpixels = med.height() * med.width();
		for(int i=0;i<this.ed_bins;i++){
			for(int j=0;j<this.gm_bins;j++){
				for(int k=0;k<this.ra_bins;k++){
					for(int l=0;l<this.tn_bins;l++){
						hist[i][j][k][l] /= tpixels;
					}	
				}
			}
		}
		
		return hist;
	}

	@Override
	public List<String> printHead() {
		int nfeatures = this.ed_bins * this.gm_bins * this.ra_bins * this.tn_bins;
		List<String> head = new ArrayList<String>(nfeatures);
			
		for(int i=1;i<=nfeatures;i++){
			head.add("@attribute feature" + i + " numeric");
		}
		
		return head;
	}

	@Override
	public String printDescriptor(String img) {
		double [][][][] hist = this.getDescriptor(img);
		
		String args = "";
		
		for(int i=0;i<this.ed_bins;i++){
			for(int j=0;j<this.gm_bins;j++){
				for(int k=0;k<this.ra_bins;k++){
					for(int l=0;l<this.tn_bins;l++){
						args = args + (hist[i][j][k][l]+",");
					}	
				}
			}
		}

		return args;
	}

}
