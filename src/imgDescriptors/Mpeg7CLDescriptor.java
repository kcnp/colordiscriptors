/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.semanticmetadata.lire.imageanalysis.mpeg7.ColorLayoutImpl;

/**
 * Color layout descriptor defined in mpeg-7.<br>
 * That descriptor wasn't implemented, instead the library Lire was used
 * @author Kaio
 * @see <a href=http://www.semanticmetadata.net/lire/> http://www.semanticmetadata.net/lire/
 */
public class Mpeg7CLDescriptor implements ImgDescriptor<Integer[]> {
	private int numberOfCCoeff;
	private int numberOfYCoeff;
	public static final int values[] = {3,6,10,15,21,28,64}; //Valores possiveis para o número de coeficientes

	private boolean isNumberOfCoefValid(int numberOfCoeff){
		for(int i=0;i<7;i++){
			if(numberOfCoeff == Mpeg7CLDescriptor.values[i]){
				return true; //Valor possivel
			}
		}
		
		return false; //Valor não possivel
	}
	
	public Mpeg7CLDescriptor(int numberOfYCoeff, int numberOfCCoeff) throws ImgDescriptorError{
		if(isNumberOfCoefValid(numberOfCCoeff) && isNumberOfCoefValid(numberOfYCoeff)){
			this.numberOfCCoeff = numberOfCCoeff;
			this.numberOfYCoeff = numberOfYCoeff;
		}else{
			throw new ImgDescriptorError("The values of numberOfCCoef and numberofYCoef " +
					"must be one of the integers in the set {3,6,10,15,21,28,64}"
			);
		}
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#getDescriptor(java.lang.String)
	 */
	@Override
	public Integer[] getDescriptor(String img) throws ImgDescriptorError{
		ColorLayoutImpl cli = null; //Object that extract the Color Layout Feature
		
		try {
			cli = new ColorLayoutImpl(
					this.numberOfYCoeff,this.numberOfCCoeff,ImageIO.read(new File(img))
			);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new ImgDescriptorError("Erro ao abrir o arquivo " + img);
		}
		
		ArrayList<Integer> clvalues = new ArrayList<Integer>(
				this.numberOfYCoeff + 2*this.numberOfCCoeff
		); //List with the coefficients that make up the descriptor
		
		int[] getCoeff;
				
		getCoeff = cli.getYCoeff();
		for(int i=0;i<this.numberOfYCoeff;i++){//luminance coefficients
			clvalues.add(getCoeff[i]);
		}
		
		getCoeff = cli.getCbCoeff();
		for(int i=0;i<this.numberOfCCoeff;i++){//blue-difference chroma coefficients
			clvalues.add(getCoeff[i]); 
		}
		
		getCoeff = cli.getCrCoeff();
		for(int i=0;i<this.numberOfCCoeff;i++){//red-difference chroma coefficients
			clvalues.add(getCoeff[i]);
		}
		
		return clvalues.toArray(new Integer[]{});
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#printHead()
	 */
	@Override
	public List<String> printHead() {
		List<String> head = new ArrayList<String>(
				this.numberOfYCoeff + 2*this.numberOfCCoeff
		);
			
		for(int i=1;i<=this.numberOfYCoeff;i++){
			head.add("@attribute YCoeff" + i + " numeric");
		}
		
		for(int i=1;i<=this.numberOfCCoeff;i++){
			head.add("@attribute CbCoeff" + i + " numeric");
		}
		
		for(int i=1;i<=this.numberOfCCoeff;i++){
			head.add("@attribute CrCoeff" + i + " numeric");
		}
		
		return head;
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#printDescriptor(java.lang.String)
	 */
	@Override
	public String printDescriptor(String img) {
		// TODO Auto-generated method stub
		Integer[] cldescriptor = this.getDescriptor(img);
		
		String args = "";
		
		for(Integer i : cldescriptor){
			args = args + i + ",";
		}
		
		return args;
	}

}
