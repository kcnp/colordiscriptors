/*******************************************************************************
 * Copyright 2013 Kaio César Nascimento Peixoto
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
/**
 * 
 */
package imgDescriptors;

import imgProcess.Segmentation;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

/**
 * @author kcnp
 *
 */
public class EdgeHistogram implements ImgDescriptor<double[][][]> {
	private int bins;
	
	/**
	 * @param bins
	 */
	public EdgeHistogram(int bins) {
		if(bins > 0 && bins <= 256){
			this.bins = bins;
		}else{
			throw new ImgDescriptorError("Bins should be in the interval [1,256]");
		}
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#getDescriptor(java.lang.String)
	 */
	@Override
	public double[][][] getDescriptor(String img) {
		Mat m = Highgui.imread(img);
		Mat norm = new Mat(); 
		Core.normalize(m,norm,0,this.bins - 1, Core.NORM_MINMAX);
		norm.convertTo(norm,CvType.CV_16U);
		
		/*Inicializa histograma*/
		double[][][] hist = new double[this.bins][this.bins][this.bins];
		for(int i=0;i<this.bins;i++){
			for(int j=0;j<this.bins;j++){
				for(int k=0;k<this.bins;k++){
					hist[i][j][k] = 0;
				}
			}
		}
		
		Mat edges = Segmentation.findEdges(norm,2);
				
		int totalEdgesPixels = 0;
		/*Calcula o histograma*/
		short[] pixel = new short[3];
		for(int j=0;j<norm.height();j++){
			for(int i=0;i<norm.width();i++){
				if(edges.get(j,i)[0] == 1){
					norm.get(j,i,pixel);
					hist[pixel[2]][pixel[1]][pixel[0]]++;
					totalEdgesPixels++;
				}
			}
		}
		
		/*Normaliza*/
		for(int i=0;i<this.bins;i++){
			for(int j=0;j<this.bins;j++){
				for(int k=0;k<this.bins;k++){
					hist[i][j][k] = hist[i][j][k] / totalEdgesPixels;
				}
			}
		}
		
		return hist;
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#printHead()
	 */
	@Override
	public List<String> printHead() {
		int ncolors = this.bins * this.bins * this.bins;
		List<String> head = new ArrayList<String>(ncolors);
			
		for(int i=1;i<=ncolors;i++){
			head.add("@attribute edge_cor" + i + " numeric");
		}
		
		return head;
	}

	/* (non-Javadoc)
	 * @see imgDescriptors.ImgDescriptor#printDescriptor(java.lang.String)
	 */
	@Override
	public String printDescriptor(String img) {
		double [][][] edge_hist = this.getDescriptor(img);
		String args = "";
		
		for(int i=0;i<this.bins;i++){
			for(int j=0;j<this.bins;j++){
				for(int k=0;k<this.bins;k++){
					args = args + (edge_hist[i][j][k]+",");
				}
			}
		}
		
		return args;
	}

}
