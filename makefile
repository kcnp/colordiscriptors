#-------------------------------------------------------------------------------
# Copyright 2013 Kaio César Nascimento Peixoto
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
LIREDIR=./lib/Lire-0.9.3
LIRELIBDIR=./lib/Lire-0.9.3/lib
LIB=./lib

CPLIRE1=$(LIREDIR)/lire.jar:$(LIRELIBDIR)/commons-math3-3.0.jar:$(LIRELIBDIR)/JOpenSurf.jar
CPLIRE2=$(LIRELIBDIR)/lucene-analyzers-common-4.0.0.jar:$(LIRELIBDIR)/lucene-core-4.0.0.jar:$(LIRELIBDIR)/lucene-queryparser-4.0.0.jar
CPLIRE=$(CPLIRE1)$(CPLIRE2)

CPLIB=$(LIB)/args4j-2.0.21.jar:$(LIB)/opencv.jar

CP=$(CPLIB):$(CPLIRE):./src/

all: clean compile

full: opencv clean compile

opencv:
	chmod u+x opencv.sh
	./opencv.sh 4 --get 
	
compile:
	mkdir bin
	javac -cp $(CP) -d bin ./src/*.java
	javac -cp $(CP) -d bin ./src/imgProcess/*.java
	javac -cp $(CP) -d bin ./src/imgDescriptors/*.java
	javac -cp $(CP) -d bin ./src/imgDescriptors/localPixelsFeatures/*.java
	ant
clean:
	rm -rf bin
	rm -rf colorDes_lib
	rm -f colorDes.jar

fclean: clean
	rm -rf opencv-2.4.5	

