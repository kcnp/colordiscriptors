#! /bin/bash

if [ -f "CorelDB.7z.*" ]
then
	rm CorelDB.7z.*
fi

wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.001
wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.002
wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.003
wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.004
wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.005
wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.006
wget https://sites.google.com/site/dctresearch/Home/content-based-image-retrieval/CorelDB.7z.007
7z x CorelDB.7z.001

rm CorelDB.7z.*
